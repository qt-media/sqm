#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <cassert>
#include <string_view>

#include <netinet/in.h>
#include <arpa/inet.h>
#include <getopt.h>

#include <RawPacket.h>
#include <Packet.h>
#include <PcapFileDevice.h>
#include <PcapPlusPlusVersion.h>
#include <SystemUtils.h>
#include <RadiusLayer.h>


#define EXIT_WITH_ERROR(reason) do { \
    std::cout << std::endl << "ERROR: " << reason << std::endl << std::endl; \
    exit(1); \
    } while(0)


void printPcapRadiusPackets(pcpp::IFileReaderDevice *reader, std::ostream &out)
{
    pcpp::RawPacket rawPacket;
    while (reader->getNextPacket(rawPacket))
    {
        pcpp::Packet parsedPacket(&rawPacket);
        auto layer = parsedPacket.getLayerOfType(pcpp::Radius);
        if (!layer)
        {
            continue;
        }

        auto radiusLayer = dynamic_cast<pcpp::RadiusLayer *>(layer);
        assert(radiusLayer);
        out << radiusLayer->toString() << " with " << radiusLayer->getAttributeCount() << " attributes \n";
        for (auto attr = radiusLayer->getFirstAttribute(); attr.isNotNull(); attr = radiusLayer->getNextAttribute(attr))
        {
            enum Fields
            {
                USER_AGENT       = 1,
                NAS_IP           = 4,
                NAS_PORT         = 5,
                FRAMED_IP        = 8,
                FILTER_ID        = 11,
                NAS_IDENTIFIER   = 32,
                ACCT_STATUS_TYPE = 40,
                ACCT_DELAY_TIME  = 41,
                ACCT_SESSION_ID  = 44,
                EVENT_TIMESTAMP  = 55,
                NAS_PORT_ID      = 87
            };

            switch (attr.getType())
            {
                case USER_AGENT:
                    out << "User-Agent: " << std::string_view((char *)attr.getValue(), attr.getDataSize()) << "\n";
                    break;

                case NAS_PORT:
                    out << "Nas-Port: " << ntohl(attr.getValueAs<uint32_t>()) << "\n";
                    break;

                case NAS_IP:
                {
                    const auto ip = attr.getValueAs<uint32_t>();
                    char ip_buff[INET_ADDRSTRLEN + 1];
                    auto conv_result = inet_ntop(AF_INET, &ip, ip_buff, sizeof(ip_buff));
                    assert(conv_result);
                    out << "Nas-Ip: " << ip_buff << "\n";
                    break;
                }

                case FRAMED_IP:
                {
                    const auto ip = attr.getValueAs<uint32_t>();
                    char ip_buff[INET_ADDRSTRLEN + 1];
                    auto conv_result = inet_ntop(AF_INET, &ip, ip_buff, sizeof(ip_buff));
                    assert(conv_result);
                    out << "Framed-Ip: " << ip_buff << "\n";
                    break;
                }

                case FILTER_ID:
                    out << "Filter-Id: " << std::string_view((char *)attr.getValue(), attr.getDataSize()) << "\n";
                    break;

                case NAS_IDENTIFIER:
                    out << "Nas-Identifier: " << std::string_view((char *)attr.getValue(), attr.getDataSize()) << "\n";
                    break;

                case ACCT_STATUS_TYPE:
                    out << "Acct-Status-Type: " << ntohl(attr.getValueAs<uint32_t>()) << "\n";
                    break;

                case ACCT_DELAY_TIME:
                    out << "Acct-Delay-Time: " << ntohl(attr.getValueAs<uint32_t>()) << "\n";
                    break;

                case ACCT_SESSION_ID:
                    out << "Acct-Sesson-Id: " << std::string_view((char *)attr.getValue(), attr.getDataSize()) << "\n";
                    break;

                case EVENT_TIMESTAMP:
                {
                    struct tm *timeinfo = nullptr;
                    time_t t = ntohl(attr.getValueAs<uint32_t>());
                    timeinfo = localtime(&t);
                    out << "Event-Timestamp: " << asctime(timeinfo);
                    break;
                }

                case NAS_PORT_ID:
                    out << "Nas-Port-Id: " << std::string_view((char *)attr.getValue(), attr.getDataSize()) << "\n";
                    break;
            }
        }
        out << "-----------------------------------------------------\n" ;
        out.flush();
    }
}

int main(int argc, char *argv[])
{
    pcpp::AppName::init(argc, argv);
    std::string inputPcapFileName;
    if (optind < argc)
    {
        inputPcapFileName = argv[optind];
    }

    if (inputPcapFileName.empty())
    {
        EXIT_WITH_ERROR("Input file name was not given");
    }

    auto reader = pcpp::IFileReaderDevice::getReader(inputPcapFileName);
    if (!reader->open())
    {
        delete reader;
        EXIT_WITH_ERROR("Error opening input pcap file\n");
    }

    printPcapRadiusPackets(reader, std::cout);

    reader->close();
    delete reader;
}
